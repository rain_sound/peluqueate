<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
      'price_service',
      'name_service',
      'time_service',
      'img_service'
    ];
}
