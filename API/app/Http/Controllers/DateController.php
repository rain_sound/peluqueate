<?php

namespace App\Http\Controllers;
use App\Date;

use Illuminate\Http\Request;

class DateController extends Controller
{
        public function index()
        {
            return Date::all();
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
          $date = Date::create($request->all());
          return response()->json($date, 201);
        }

        /**
         * Display the specified resource.
         *
         * @param  \App\Date  $date
         * @return \Illuminate\Http\Response
         */
        public function show(Date $date)
        {
            return $date;
        }
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \App\Date  $date
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, Date $date)
        {
            $date->update($request->all());
            return response()->json($date, 200);
        }

}
