<?php

namespace App\Http\Controllers;
use App\Barber;

use Illuminate\Http\Request;

class BarberController extends Controller
{
        public function index()
        {
            return Barber::all();
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
          $barber = Barber::create($request->all());
          return response()->json($barber, 201);
        }

        /**
         * Display the specified resource.
         *
         * @param  \App\Barber  $barber
         * @return \Illuminate\Http\Response
         */
        public function show(Barber $barber)
        {
            return $barber;
        }
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \App\Barber  $barber
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, Barber $barber)
        {
            $barber->update($request->all());
            return response()->json($barber, 200);
        }

}
