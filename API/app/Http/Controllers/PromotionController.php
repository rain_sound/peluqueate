<?php

namespace App\Http\Controllers;
use App\Promotion;

use Illuminate\Http\Request;

class PromotionController extends Controller
{
        public function index()
        {
            return Promotion::all();
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
          $promotion = Promotion::create($request->all());
          return response()->json($promotion, 201);
        }

        /**
         * Display the specified resource.
         *
         * @param  \App\Promotion  $promotion
         * @return \Illuminate\Http\Response
         */
        public function show(Promotion $promotion)
        {
            return $promotion;
        }
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \App\Promotion  $promotion
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, Promotion $promotion)
        {
            $promotion->update($request->all());
            return response()->json($promotion, 200);
        }

}
