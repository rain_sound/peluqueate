<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterAuthRequest;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use JWTAuth;

class ApiController extends Controller
{
  public $loginAfterSignUp = false;

      public function register(RegisterAuthRequest $request)
      {
          try {
            $user = new User();
            $user->name_user = $request->name_user;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->address_user = $request->address_user;
            $user->type_user = $request->type_user;
            $user->cell_user = $request->cell_user;
            $user->save();
            if ($this->loginAfterSignUp) {
                return $this->login($request);
            }
            return response()->json([
                'success' => true,
                'data' => $user
            ], 200);
          }
          catch (\Exception $e) {
            echo $e->getMessage();
          }
      }

      public function login(Request $request)
      {
          $input = $request->only('email', 'password');
          $jwt_token = null;
          echo "sdsd";
          try {
            if (!$token = JWTAuth::attempt($input)) { // attempt to verify the credentials and create a token for the user
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
          } catch (JWTException $e) {
              return response()->json(['error' => 'could_not_create_token'], 500); // something went wrong whilst attempting to encode the token
          }

          return response()->json(['success' => true,'token' => "Bearer $token"]);

          // if (!$jwt_token = JWTAuth::attempt($input)) {
          //     return response()->json([
          //         'success' => $jwt_token,
          //         'message' => 'Error en las credenciales de usuarios',
          //     ], 400);
          // }


      }

      public function logout(Request $request)
      {
          $this->validate($request, [
              'token' => 'required'
          ]);

          try {
              JWTAuth::invalidate($request->token);

              return response()->json([
                  'success' => true,
                  'message' => 'User logged out successfully'
              ]);
          } catch (JWTException $exception) {
              return response()->json([
                  'success' => false,
                  'message' => 'Sorry, the user cannot be logged out'
              ], 500);
          }
      }

      public function getAuthUser(Request $request)
      {
          $this->validate($request, [
              'token' => 'required'
          ]);

          $user = JWTAuth::authenticate($request->token);

          return response()->json(['user' => $user]);
      }
  }
