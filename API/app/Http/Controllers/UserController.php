<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use JWTAuth;

class UserController extends Controller
{
    protected $user;

    public function __construct()
    {
    }

    public function index(){
      return User::all();
    }

    public function show(User $user){
      return $user;
    }

    public function store(Request $req){
      $user = User::create($req->all());
      return response()->json($user, 201);
    }

    public function update(Request $req, User $user){
      $user->update($req->all());
      return response()->json($user, 200);
    }

    //Se declara como metodo pero se accede como propiedad
    //orders declarado en User.model
    //Es utilizado para establecer la relacion
    public function showByUser($id){
      $orders = [];
      $orders = User::find($id)->orders;
      if (!$orders) {
       return response()->json([
               'success' => false,
               'message' => 'Sorry, product with id ' . $id . ' cannot be found'
           ], 400);
       }
      return $orders;
    }

}
