<?php

namespace App\Http\Controllers;
use App\Picture;

use Illuminate\Http\Request;

class PictureController extends Controller
{
        public function index()
        {
            return Picture::all();
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
          $picture = Picture::create($request->all());
          return response()->json($picture, 201);
        }

        /**
         * Display the specified resource.
         *
         * @param  \App\Picture  $picture
         * @return \Illuminate\Http\Response
         */
        public function show(Picture $picture)
        {
            return $picture;
        }
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \App\Picture  $picture
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, Picture $picture)
        {
            $picture->update($request->all());
            return response()->json($picture, 200);
        }

}
