<?php

namespace App\Http\Controllers;
use App\Service;

use Illuminate\Http\Request;

class ServiceController extends Controller
{
        public function index()
        {
            return Service::all();
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
          $service = Service::create($request->all());
          return response()->json($service, 201);
        }

        /**
         * Display the specified resource.
         *
         * @param  \App\Service  $service
         * @return \Illuminate\Http\Response
         */
        public function show(Service $service)
        {
            return $service;
        }
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \App\Service  $service
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, Service $service)
        {
            $service->update($request->all());
            return response()->json($service, 200);
        }

}
