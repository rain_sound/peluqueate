<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterAuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
         'name_user' => 'required|string',
         'email' => 'required|string',
         'password' => 'required|string',
         'address_user' => 'required|string',
         'type_user' => 'required|integer',
         'cell_user' => 'string',
     ];
    }
}
