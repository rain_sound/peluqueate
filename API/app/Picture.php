<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $fillable = [
      'date_picture',
      'url_picture'
    ];


   public function user(){
     return $this->belongsTo('App\User');
   }

}
