<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
      'name_product',
      'desc_product',
      'img_product'
    ];
}
