<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
    protected $fillable = [
      'desc_date'
    ];

    public function user(){
      return $this->belongsTo('App\User');
    }

}
