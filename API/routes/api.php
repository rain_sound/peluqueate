<?php

use Illuminate\Http\Request;
use App\User;

Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');

Route::group(['middleware' => 'jwt.auth'], function () {
  Route::apiResource('products','ProductController');
  Route::apiResource('dates',"DateController");
  Route::apiResource('users','UserController');
  Route::apiResource('promotions','PromotionController');
  Route::apiResource('services','ServiceController');
  Route::apiResource('pictures','PictureController');
  Route::apiResource('barbers','BarberController');

  // Route::apiResource('users','UserController');
});
//     Route::post('users/{id}/orders','UserController@showByUser');
//     Route::get('logout', 'ApiController@logout');
//     Route::get('user', 'ApiController@getAuthUser');
// });
