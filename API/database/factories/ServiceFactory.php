<?php

use Faker\Generator as Faker;

$factory->define(App\Service::class, function (Faker $faker) {
    return [
        'price_service' => $faker->randomNumber(2),
        'name_service' => $faker->lexify('Service ???'),
        'time_service' => $faker->numberBetween($min = 0, $max = 20),
        'img_service' => $faker->imageUrl
    ];
});
