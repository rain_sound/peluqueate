<?php

use Faker\Generator as Faker;

$factory->define(App\Picture::class, function (Faker $faker) {
    return [
        'date_picture' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'url_picture' => $faker->imageUrl
    ];
});
