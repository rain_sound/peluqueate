<?php

use Faker\Generator as Faker;

$factory->define(App\Date::class, function (Faker $faker) {
    return [
        'desc_date' => $faker->lexify('Date ???'),
    ];
});
