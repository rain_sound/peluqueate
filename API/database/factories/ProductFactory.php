<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name_product' => $faker->name,
        'desc_product' => $faker->lexify('Product ???'),
        'img_product' => $faker->imageUrl
    ];
});
