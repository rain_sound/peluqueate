<?php

use Faker\Generator as Faker;

$factory->define(App\Barber::class, function (Faker $faker) {
    return [
        'name_barber' => $faker->name,
        'type_barber' => $faker->numberBetween($min = 0, $max = 1)
    ];
});
