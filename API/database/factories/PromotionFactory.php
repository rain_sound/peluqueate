<?php

use Faker\Generator as Faker;

$factory->define(App\Promotion::class, function (Faker $faker) {
    return [
        'desc_promotion' => $faker->lexify('Promotion ???'),
        'date_promotion' => $faker->date($format = 'Y-m-d', $max = 'now')
    ];
});
