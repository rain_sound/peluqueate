<?php

use Illuminate\Database\Seeder;

class DateSeeder extends Seeder
{
    public function run()
    {
        factory(App\Date::class,8)
          ->create();
    }
}
