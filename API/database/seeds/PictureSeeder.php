<?php

use Illuminate\Database\Seeder;

class PictureSeeder extends Seeder
{
    public function run()
    {
        factory(App\Picture::class,8)
          ->create();
    }
}
