<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(BarberSeeder::class);
         $this->call(ProductSeeder::class);
         $this->call(PromotionSeeder::class);
         $this->call(ServiceSeeder::class);
         $this->call(UsersSeeder::class);
    }
}
