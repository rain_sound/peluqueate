<?php

use Illuminate\Database\Seeder;

class BarberSeeder extends Seeder
{
    public function run()
    {
        factory(App\Barber::class,8)
          ->create();
    }
}
