<?php

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    public function run()
    {
        factory(App\Service::class,8)
          ->create();
    }
}
