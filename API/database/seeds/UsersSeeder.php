<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run()
    {
        factory(App\User::class,6)
          ->create()
          ->each( function($u) {
            $u->pictures()->save(factory(App\Picture::class)->make());
          })
          ->each( function($u) {
            $u->dates()->save(factory(App\Date::class)->make());
          });
    }
}
