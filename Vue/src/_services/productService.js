import api from '../api';


export default function getAllProducts() {
  return api.get('products')
    .then((response) => {
      console.log(response.data);
    })
    .catch((error) => {
      console.log(error);
    })
}
