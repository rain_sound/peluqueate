import authHeader from '../_helpers/authHeader';
import api from '../api';

export const userService = {
  login, logout
};

function login(email, password) {
  return api.post('login', {
      email: email,
      password: password
    })
      .then((response) => {
        localStorage.setItem('user', JSON.stringify(response.data));
        const user = localStorage.getItem('user');
        console.log(response.data);
        return response.data;
      });
}

function logout(){
  localStorage.removeItem('user');
}
