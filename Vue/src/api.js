import axios from 'axios'
import { authHeader } from './_helpers/authHeader'

//Base url api rest
//middleware
export default axios.create({
  baseURL: 'http://localhost:8000/api',
  headers: authHeader
});
