import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from './_helpers/router.js'
import { store } from './_store'

//Url del backend
// axios.defaults.baseUrl = 'http://localhost:8000/api'

//Usando middleware
Vue.use(BootstrapVue, VueAxios, axios)

//Creando instancia vue, inyectando router
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
