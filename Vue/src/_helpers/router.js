import Vue from 'vue'
import Router from 'vue-router'
import Home from 'Views/Home/index.vue'
import Login from 'Views/Login/index.vue'
import Products from 'Views/Products/index.vue'
import Dashboard from 'Views/Dashboard/index.vue'
import NewProduct from 'Views/NewProduct/index.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/products',
      name: 'Products',
      component: Products
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/new/product',
      name: 'new-product',
      component: NewProduct
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    }
  ]
});


router.beforeEach((to, from, next) => {
  const publicPages = ['/login','/register'];
  const authRequired = publicPages.includes(to.path);
  // const loggedIn = localStorage.getItem('user');
  const loggedIn = null;


  if(!authRequired && !loggedIn) {
    next('/login');
    console.log('aaa');
  }
  next();
});
//Primero se debe de declarar la variable y despues
//exportar


export default router;
