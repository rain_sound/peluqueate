const state = {
  type: null,
  message: null
}

const actions = {
  success({ commit }, message) {
    commit('sucess', message);
  },
  error({ commit }, message) {
    commit('error', message);
  },
  clear({ commit }, message) {
    commit('clear', message);
  }
}

const mutations = {
  success(state, message) {
    state.type = 'alert-sucess';
    state.message = message;
  },
  error(state, message) {
    state.type = 'alert-error';
    state.message = message;
  },
  clear(state) {
    state.type = null,
    state.message = null
  }
}

export const alert = {
  namespaced: true,
  state,
  actions,
  mutations
};
