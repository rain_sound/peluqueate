import { userService } from '../_services';
import router  from '../_helpers/router';

const user = JSON.parse(localStorage.getItem('user'));
const state = user
            ? { status: { loggedIn: true }, user }
            : { status: {}, user: null };


const actions = {
  login({ distpach, commit },{ email, password }){
    commit('loginRequest', { email });
    userService.login(email, password)
      .then((response) => {
          console.log('account');
          commit('loginSuccess', response);
          router.push('/');
         }
       );
  },
  logout({ commit }) {
    userService.logout();
    commit('logOut');
  }
}

const mutations = {
  loginRequest(state, user) {
    state.status = { loggingIn: true };
    state.user = user;
  },
  loginSuccess(state, user) {
    state.status = { loggedIn: true };
    state.user = user;
  },
  loginFailure(state) {
    state.status = { loggedIn: false };
    state.user = null;
  },
  logOut(state) {
    state.status = {};
    state.user = null;
  },

}


export const account = {
  namespaced: true,
  state,
  actions,
  mutations
}
