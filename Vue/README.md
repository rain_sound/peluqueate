# my-project

> example

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

# REFERENCES

https://coursetro.com/posts/code/139/Vue-Router-Tutorial---Using-Vue's-Router-Library

# STRUCTURE

https://vuejsdevelopers.com/2017/03/24/vue-js-component-templates/

# DOCS

https://vuejs.org/v2/api/?

# VUE ROUTER

https://router.vuejs.org/guide/#javascript

# BOOTSTRAP VUE

https://bootstrap-vue.js.org/docs/components/button#router-link-support

# STRUCTURE

```
src
    ├── assets            Recursos
    │   ├── media         Imagenes/Videos
    │   └── scss          Archivos scss
    ├── components        Componentes
    │   └── Carousel      Ejemplo componente (carpeta)
    ├── routes            Archivo de rutas
    └── views             Carpeta de vistas
        ├── Home          Ejemplo de una pagina principal
        └── Products
```
